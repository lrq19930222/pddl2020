# Novelty Description Language

An unsupervised learning method for novelty detection and characterization based on qualitative spatial relations.

More detailed description for this project can be found in section 2.2 of https://docs.google.com/document/d/1wiDCTdh4-YgWXjDehzEgPcZ2FSHhuzVtTzX7Ktd40ZM/edit?usp=sharing.


## Requirements

Python 3.6+

## Run the pipeline in sequence:

Run **run_all.sh** for the whole pipeline:

- **Input**: GeoJson formatted groundtruth files.

- **GetQSRRelations.py**: Generate 4 QSR features to present the state transition, including RCC, QDC, STAR-4, and QTC (QTC.py should run seperately after).
- **Existence.py**: Generate one additional existence feature.
- **Read_Data_consequence.py**: Filter state transitions that at least one feature has changed.
- **Data_preprocessing.py**: Concatenate state traisitions and convert them into clustering-welcomed format.
- **Hierarchical_clustering.py**: Perform hierarchical clustering on the state transitions.

- **Output**: TXT format clusters.

Option: To run test, run novelty_char.ipynb on Jupyter.

## Citing this Work

If you use this method in your research, please cite:

Paper is accepted by KR2021 conference.

