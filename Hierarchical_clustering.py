import pandas as pd
import sys
import argparse
import itertools
import numpy as np
import pickle
from collections import Counter
from multiprocessing import Pool
import matplotlib.pyplot as plt
import time
import json
import datetime
from pathlib import Path
from qsr.Distance_neighborhood import calculate_neighborhood_distance
from Graph_Clustering import get_cluster


def get_objpair_type(df):
    """
    Get pure object types of each object pair by removing the ID
    :param df: data
    :return: example: before: -33722_ice_rect_fat_1*-33978_stone_rect_fat_1; after: ice_rect_fat_1*_stone_rect_fat_1
    """
    s = df.split("*")
    if any(map(str.isdigit, s[0].split("_")[-1])):
        obj1 = '_'.join(s[0].split("_")[1: -1])
    else:
        obj1 = '_'.join(s[0].split("_")[1:])
    if any(map(str.isdigit, s[0].split("_")[-1])):
        obj2 = '_'.join(s[1].split("_")[1: -1])
    else:
        obj2 = '_'.join(s[1].split("_")[1:])

    return obj1 + "*" + obj2


def convert_to_list(df):
    """
    convert the dataframe to list of list for clustering
    :param df: dataframe
    :return: data rows represented by list of list of list
    """
    row_list = [ ]
    for index, rows in df.iterrows():
        my_list = [
            [ rows.rcc_diff, rows.direct_diff, rows.dist_diff, rows.exist_diff, rows.qtc_diff, rows.objpair_type ] ]
        row_list.append(my_list)
    return row_list

def check_thres_M_merge(data, thres, M):
    """
    check if two clusters are qualified to be merged, if yes, return their similarity, else return False
    :param data: pre-merged cluster from initial_cluster_i and initial_cluster_j
    :param thres: \tau predefined by users to restrict the merging
    :param M: \delta predefined by users to restrict the merging
    :return: if satisfied \tau and \delta, return similarity, else return False
    """
    count = 0
    percent = []
    for j in range(5):
        l = [item[j] for item in data]
        f = Counter(l).most_common(1)[0]
        if f[1] >= thres*len(data):
            count +=1
            percent.append(f[1])
    if count >= M:
        percent.sort(reverse=True)
        result = sum(percent[:M])/(M*len(data))
        return '%.2f' % result
    else:
        return False


def hierachical_clustering(data1, data2):
    """
    Apply hierachical clustering on initial clusters/STs
    :param data1: cluster1
    :param data2: cluster2
    :return: cluster1 and cluster2 and distance between them
    """
    if data1[0][-1] != data2[0][-1]:
        return 0
    else:
        sum_distance = check_thres_M_merge(data1+data2, 0.95, 5)

        if sum_distance:
            return sum_distance
        else:
            return 0


def merge_duplicated_data(df):
    """
    merge identical state transitions
    :param df: state transitions dataframe
    :return: initial clusters after merging
    """
    df_noduplicated = df.drop_duplicates(keep=False)
    duplicateRowsDF = df[df.duplicated(keep=False)]
    duplicateRowsDF['size'] = duplicateRowsDF.groupby(["rcc_diff", "direct_diff", "dist_diff", "exist_diff", "qtc_diff", "objpair_type"])['rcc_diff'].transform('size')
    duplicateRowsDF = duplicateRowsDF.drop_duplicates()
    duplicateRowsls = duplicateRowsDF.values.tolist()
    duplicated_clusters = []
    for i in duplicateRowsls:
        temp = []
        for j in range(i[-1]):
            temp.append(i[:-1])
        duplicated_clusters.append(temp)
    noduplicated_clusters = [[i] for i in df_noduplicated.values.tolist()]
    
    return duplicated_clusters + noduplicated_clusters


def cached_clustering(data, initial_dist_dict, plot=True):
    """
    do hierarchical clustering by caching distance among cluster pairs
    :param data: dict of clusters (for recording indices of the clusters as keys)
    :param initial_dist_dict: the dict that stores the distance values of all possible cluster pairs from the initial clusters
    :param plot: if want to plot the clustering procedure
    :return: clusters generated from hierarchical clusterinng (saved in txt files)
    """
    v = list(initial_dist_dict.values())
    if max(v) != 0:
        max_pair = list(initial_dist_dict.keys())[v.index(max(v))]
    else:
        sys.exit("No similar clusters found!")

    # remove all the distance with at least one element is from the two removed clusters
    for k in initial_dist_dict.copy():
        if max_pair[0] in k or max_pair[1] in k:
            del initial_dist_dict[k]

    cluster_merge = data[max_pair[0]] + data[max_pair[1]]
    initial_max_index = max(data)
    data[initial_max_index+1] = cluster_merge
    del data[max_pair[0]]
    del data[max_pair[1]]

    for d in range(total_num):
        max_index = max(data)
        for i in data.keys():
            if i != max_index:
                dist = hierachical_clustering(data[i], data[max_index])
                if dist != 0:
                    initial_dist_dict[(i, max_index)] = dist
        v = list(initial_dist_dict.values())

        if v:
            max_dist = max(v)
            max_pair = list(initial_dist_dict.keys())[v.index(max_dist)]

            # remove all the distance with at least one element is from the two removed clusters
            for k in initial_dist_dict.copy():
                if max_pair[0] in k or max_pair[1] in k:
                    del initial_dist_dict[k]
            cluster_merge = data[max_pair[0]] + data[max_pair[1]]
            data[max_index+1] = cluster_merge
            del data[max_pair[0]]
            del data[max_pair[1]]

            print("The size of original dataset is {}, now is {}, max distance is {}, dict size is {}".format(total_num, len(new_data), max_dist, len(initial_dist_dict)), "Used time: {}".format(time.time() - start_time))
            X.append(len(new_data))
            Y.append(max_dist)
        else:
            with open(write_path + "data_{}.txt".format(len(new_data)), "wb") as fp:   #Pickling
                pickle.dump(list(new_data.values()), fp)
            print("The size of original dataset is {}, now is {}".format(total_num, len(new_data)), "Used time: {}".format(time.time() - start_time))
            # with open(write_path + "X.txt", "wb") as fp:   #Pickling
            #     pickle.dump(X, fp)
            # with open(write_path + "Y.txt", "wb") as fp:   #Pickling
            #     pickle.dump(Y, fp)

    if plot:
            plt.plot(X, Y, 'ro-')
            plt.title('The shortest distance and clusters')
            plt.xlabel('number of clusters')
            plt.ylabel('Shortest distance')
            plt.axis([max(X),min(X),min(Y),max(Y)])
            plt.show()
            print("Finish clustering at: ", datetime.datetime.now())
            sys.exit("No similar clusters found!")




if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Clustering')
    parser.add_argument('-r', '--readpath', help='Specify STs path', required=True)
    parser.add_argument('-w', '--writepath', help='Specify clusters write path', required=True)
    args = parser.parse_args()

    ST_path = args.readpath
    write_path = args.writepath
    Path(write_path).mkdir(parents=True, exist_ok=True)
    df = pd.read_csv(ST_path)
    df[ "objpair_type" ] = df[ "objectid_pair" ].apply(lambda x: get_objpair_type(x))
    print(df.shape)
    df = df[df['objpair_type'].str.contains("bird")]
    df_unique = df.drop(columns=["objectid_pair", 'temporal_start', 'temporal_end'])
    print("Initial data size is {}".format(df_unique.shape))
    print("start initial clustering at: ", datetime.datetime.now())


    if not Path(write_path + 'initial_clusters.pkl'):
        data = merge_duplicated_data(df_unique)
        data = convert_to_list(df_unique)
        data = get_cluster(data, 6, 1)
        # print("After merge duplicates, the data size is {}".format(len(data)))
        print("finish initial clustering at: ", datetime.datetime.now())

        with open(write_path + 'initial_clusters.pkl', 'wb') as f:
            pickle.dump(data, f)
    else:
        with open(write_path + 'initial_clusters.pkl', 'rb') as f:
            data = pickle.load(f)


    new_data = {}
    for d in range(len(data)):
        new_data[d] = data[d]
    total_num = len(new_data)
    print(total_num)
    X = []
    Y = []
    if not Path(write_path + 'initial_dist_dict.pkl'):
        # Initially calculate the distance of all the initial clusters
        initial_dist_dict = {}
        start_time = time.time()

        for i in range(len(data) - 1):
            for j in range(i+1, len(data)):
                dist = hierachical_clustering(data[i], data[j])
                if dist != 0:
                    initial_dist_dict[(i, j)] = dist

        with open(write_path + 'initial_dist_dict.pkl', 'wb') as f:
            pickle.dump(initial_dist_dict, f)
    else:
        with open(write_path + 'initial_dist_dict.pkl', 'rb') as f:
            initial_dist_dict = pickle.load(f)


    cached_clustering(new_data, initial_dist_dict)











