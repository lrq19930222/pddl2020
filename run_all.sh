
#!/bin/bash
source ~/.bash_profile
set -e

gt_path = "/home/richie/Documents/sciencebirdsframework-release-alpha0.4.1/ScienceBirds/linux/gt/"
feature_write_path = "/home/richie/Desktop/pddl2020/geojson/gt_batch_features/"
state_write_path = "/home/richie/Desktop/pddl2020/geojson/gt_batch_states/"
ST_write_path = "/home/richie/Desktop/pddl2020/geojson/gt_batch_STs.csv"
cluster_write_path = "/home/richie/Desktop/pddl2020/geojson/gt_batch_M_5_T_99/"


echo "Start to generate 4 QSR + 1 existence features from the ground truth files"
python GetQSRRelations.py -r $gt_path -w $feature_write_path || true
python qsr.qmodels.QTC.py -w $feature_write_path || true
python qsr.qmodels.Existence.py -r $gt_path -w $feature_write_path || true

echo "Finish generating features, start extract state transitions"
python Read_Data_consequence.py -r $feature_write_path -w $state_write_path || true
python Data_preprocessing.py -r $state_write_path -w $ST_write_path || true

echo "Start clustering STs"
python Hierarchical_clustering.py || true
echo "Finish"
