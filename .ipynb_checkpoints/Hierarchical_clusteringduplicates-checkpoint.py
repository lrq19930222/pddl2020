import pandas as pd
import math
import itertools
import numpy as np
import numba as nb
import pickle
from multiprocessing import Pool
import matplotlib.pyplot as plt
import time
from qsr.Distance_neighborhood import calculate_neighborhood_distance
from Graph_Clustering import get_cluster


def get_objpair_type(df):
	"""
    Get pure object types of each object pair by removing the ID
    :param df: data
    :return: before: -33722_ice_rect_fat_1*-33978_stone_rect_fat_1; after: ice_rect_fat*_stone_rect_fat
    """
	s = df.split("*")
	obj1 = '_'.join(s[0].split("_")[1: -1])
	obj2 = '_'.join(s[1].split("_")[1: -1])
	return obj1 + "*" + obj2


def convert_to_list(df):
	"""
	convert the dataframe to list of list for clustering
	:param df: dataframe
	:return: data rows represented by list of list of list
	"""
	row_list = [ ]
	for index, rows in df.iterrows():
		my_list = [
			[ rows.rcc_diff, rows.direct_diff, rows.dist_diff, rows.exist_diff, rows.qtc_diff, rows.objpair_type ] ]
		row_list.append(my_list)
	return row_list


def hierachical_clustering(data1, data2):
	"""
	Apply hierachical clustering on raw/graph clusterred data
	:param data1: cluster1
	:param data2: cluster2
	:return: cluster1 and cluster2 and distance between them
	"""
	combination = list(itertools.product(data1, data2))
	sum_distance = 0
	for c in combination:
		if c[0][-1] != c[1][-1]:
			sum_distance = math.inf
			break
		else:
			sum_distance += calculate_neighborhood_distance(c[0], c[1])
	avg_distance = sum_distance / (len(data1) * len(data2))

	return avg_distance, data1, data2


if __name__ == '__main__':
	df = pd.read_csv("/home/richie/Desktop/pddl/data/temp_170_3_merge.csv")
	df[ "objpair_type" ] = df[ "objectid_pair" ].apply(lambda x: get_objpair_type(x))
	df_unique = df
	print("Initial data size is {}".format(df.shape))
	data = convert_to_list(df_unique)
	data = get_cluster(data, 6)
	print("After merge duplicates, the data size is {}".format(len(data)))
	data = get_cluster(data, 5)
	print(" Total {} data for clustering".format(len(data)))
	total_num = len(data)
	X = []
	Y = []
	distance = []
	for i in range(total_num - 1):
		start_time = time.time()
		best_distance = math.inf
		# index_list = [count for count in range(len(data))]
		# pair_index = list(itertools.combinations(index_list, 2))
		# pair_data = list(itertools.combinations(data, 2))
		for j in range(len(data)-1):
			combination = list(itertools.product([data[j]], data[j+1:]))
			# for m in range(j+1, len(data)):
			# parallel processing
			pool = Pool(processes=12)
			# input = [data[j], data[m]]
			overall_distance = pool.starmap(hierachical_clustering, combination)
			pool.close()
			pool.join()
			# select the shortest distance
			temp = next(zip(*overall_distance))
			distance = overall_distance[temp.index(min(temp))]
			# distance = hierachical_clustering([data[j], data[m]])
			if best_distance > distance[0]:
				best_distance = distance[0]
				best_index = [data.index(distance[1]), data.index(distance[2])]
				print(best_distance, best_index)
			if best_distance <= 0.125:
				break
		if best_distance == math.inf:
			break
		print("Finished {}th round, the shortest distance is {}, the merged data is {} and {}".format(i, best_distance, len(data[best_index[0]]), len(data[best_index[1]])))
		new_element = data[best_index[0]] + data[best_index[1]]
		del data[max(best_index[0], best_index[1])]
		del data[min(best_index[0], best_index[1])]
		data.append(new_element)
		X.append(len(data))
		Y.append(best_distance)
		# if best_distance >= 2:
		with open("/home/richie/Desktop/result/result_170_3_duplicates/data_{}_{}.txt".format(round(best_distance,3), len(data)), "wb") as fp:   #Pickling
			pickle.dump(data, fp)
		print("The size of original dataset is {}, now is {}".format(total_num, len(data)), "Used time: {}".format(time.time() - start_time))



	with open("/home/richie/Desktop/result/result_170_3_duplicates/X.txt", "wb") as fp:   #Pickling
		pickle.dump(X, fp)
	with open("/home/richie/Desktop/result/result_170_3_duplicates/Y.txt", "wb") as fp:   #Pickling
		pickle.dump(Y, fp)

	plt.plot(X, Y, 'ro-')
	plt.title('The shortest distance and clusters')
	plt.xlabel('number of clusters')
	plt.ylabel('Shortest distance')
	plt.axis([max(X),min(X),min(Y),max(Y)])
	plt.show()
