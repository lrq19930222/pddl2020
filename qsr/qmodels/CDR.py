from __future__ import print_function, division
from shapely.geometry import Polygon
from math import radians, cos, sin, degrees, atan2, sqrt, asin, pi, floor

def  CDRRelation(poly1, poly2):
    """Calculate CDR relation of two polygons
    :param poly1: coordinate of polygon1 [[x1, y1], [x2, y2], ....[xn, yn]]
    :param poly2: coordinate of polygon2 [[x1, y1], [x2, y2], ....[xn, yn]]
    :return: CDR relation
    """
    area_dict_poly1 = get_CDR_area(poly1)
    poly2 = Polygon(poly2)
    CDR_relation = ""
    for key, value in area_dict_poly1.items():
        value = Polygon(value)
        if value.intersects(poly2):
            CDR_relation = CDR_relation + key + ":"
    return CDR_relation[:-1]


def get_min_max_by_cordinate(poly):
    """ Get x_min, x_max, y_min, y_max of polygon
    :param poly: coordinate of polygon1 [[x1, y1], [x2, y2], ....[xn, yn]]
    :return: x_min, x_max, y_min, y_max
    """
    x_min = min(poly, key=lambda x: x[0])[0]
    x_max = max(poly, key=lambda x: x[0])[0]
    y_min = min(poly, key=lambda x: x[1])[1]
    y_max = max(poly, key=lambda x: x[1])[1]
    return x_min, x_max, y_min, y_max

def get_CDR_area(poly):
    """ Get all-around areas of poly
    :param poly: coordinate of polygon1 [[x1, y1], [x2, y2], ....[xn, yn]]
    :return: dictionary of 9 surrounding area's vertices
    """
    screen_width = 840
    screen_height = 480
    x_min, x_max, y_min, y_max = get_min_max_by_cordinate(poly)

    area_dict = {}
    area_dict["NW"] = [[0, screen_height], [x_min, screen_height], [x_min, y_max], [0, y_max]]
    area_dict["N"] = [[x_min, screen_height], [x_max, screen_height], [x_max, y_max], [x_min, y_max]]
    area_dict["NE"] = [[x_max, screen_height], [screen_width, screen_height], [screen_width, y_max], [x_max, y_max]]
    area_dict["W"] = [[0, y_max], [x_min, y_max], [x_min, y_min], [0, y_min]]
    area_dict["B"] = [[x_min, y_max], [x_max, y_max], [x_max, y_min], [x_min, y_min]]
    area_dict["E"] = [[x_max, y_max], [screen_width, y_max], [screen_width, y_min], [x_max, y_min]]
    area_dict["SW"] = [[0, y_min], [x_min, y_min], [x_min, 0], [0, 0]]
    area_dict["S"] = [[x_min, y_min], [x_max, y_min], [x_max, 0], [x_min, 0]]
    area_dict["SE"] = [[x_max, y_min], [screen_width, y_min], [screen_width, 0], [x_max, 0]]
    
    return area_dict



if __name__ == '__main__':
    m1 = [[2, 4], [2, 8], [6, 8], [6, 4]]
    m2 = [[3, 5], [3, 7], [5, 7], [5, 5]]

    print(CDRRelation(m1, m2))

