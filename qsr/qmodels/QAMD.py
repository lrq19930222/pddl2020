import time
import os
import json
import math
from numpy import linalg as LA
import numpy as np
from math import radians, cos, sin, degrees, atan2, sqrt, asin, pi, floor



def  directionRelations(pos, center1, center2):
    """Calculate direction and distance relation of two polygons
    :param poly1: coordinate of polygon1 [[x1, y1], [x2, y2], ....[xn, yn]]
    :param poly2: coordinate of polygon2 [[x1, y1], [x2, y2], ....[xn, yn]]
    :return: derection relation index
    """
    # center1, center2 = center_geolocation(poly1), center_geolocation(poly2)
    # Finds the differnece between the centres of each polygon
    dx = center1[0] - center2[0]
    dy = center1[1] - center2[1]

    if dx == 0 and dy == 0:
        direction = 'eq'
    else:
        # Calculate the angle of the line between the two objects (in degrees)
        angle = (atan2(dx,dy) * (180/pi))+22.5
        # If that angle is negative, invert it
        if angle < 0.0:
            angle = (360.0 + angle)
        angle_direct = {0: [7, 8], 1: [9, 10], 2: [11, 12], 3: [13, 14], 4: [0, 15, 16], 5: [1, 2], 6: [3, 4], 7: [5, 6]}
        for k, v in angle_direct.items():
            if floor((angle / 22.5)) in v:
                direction = __direction_switch(pos, k)

    # Lookup labels and return answer
    return direction

def __direction_switch(pos,x):
    """Switch Statement convert number into region label
    :param derection relation index
    :return: QSR relation.
    """
    if pos == "left":
        return {
            0: '4',
            1: '5',
            2: '6',
            3: '7',
            4: '0',
            5: '1',
            6: '2',
            7: '3',
        }.get(x)
    else:
        return {
            0: '4',
            1: '3',
            2: '2',
            3: '1',
            4: '0',
            5: '7',
            6: '6',
            7: '5',
        }.get(x)

def QAMD_pq(data):
    """
    QAMD represents the 8-way 1D relative motion of two points.
    """
    p = np.array(data[0][0:2])
    q = np.array(data[0][2:4])
    pn = np.array(data[1][0:2])
    qn = np.array(data[1][2:4])

    ###check p is on the right/left to q#########
    if p[0] <= q[0]:
        #left
        qamd_p = directionRelations("left", p, pn)
        qamd_q = directionRelations("right", q, qn)
    else:
        #right
        qamd_p = directionRelations("right", p, pn)
        qamd_q = directionRelations("left", q, qn)

    return [qamd_p, qamd_q]

start_time = time.time()
data_dir = "/home/richie/Desktop/pddl/geojson/gt_batch_new_split_test_3_merged_random/"
all_data = os.listdir(data_dir)
for level in all_data:
    index_list = []
    one_level = os.listdir(data_dir+level)
    for js in one_level:
        if "_center.json" in js:
            index_list.append(int(js.split("_")[0]))
    start = min(index_list)
    end = max(index_list)
    for ind in range(end, start, -1):
        print("start", ind)
        with open(data_dir + level + "/" + "{}_center.json".format(ind)) as cur_cent:
            curcent_json = json.load(cur_cent)
        with open(data_dir + level + "/" + "{}_center.json".format(ind-1)) as pre_cent:
            precent_json = json.load(pre_cent)

        for key, value in curcent_json.items():
            if key in precent_json.keys():
                curcent_json[key] = QAMD_pq([precent_json[key], value])
            else:
                curcent_json[key] = QAMD_pq([value, value])
        # os.remove(data_dir + level + "/" + "{}_center.json".format(ind))
        with open(data_dir + level + "/" + "{}_qamd.json".format(ind), 'w') as fp:
            fp.write(json.dumps(curcent_json, sort_keys=True))
    with open(data_dir + level + "/" + "{}_center.json".format(start)) as start_cent:
            stacent_json = json.load(start_cent)
    for key, value in stacent_json.items():
            stacent_json[key] = QAMD_pq([value, value])
    # os.remove(data_dir + level + "/" + "{}_center.json".format(start))
    with open(data_dir + level + "/" + "{}_qamd.json".format(start), 'w') as fp:
        fp.write(json.dumps(stacent_json, sort_keys=True))


