from __future__ import print_function, division
from shapely.geometry import Polygon


def QASpARelation(velo1, velo2):
	"""Calculate QASp relation of two polygons
	:param velo1: angular velocity value of polygon1
    :param velo2: angular velocity value of polygon2
	:return: QASp relation
	"""
	
	# cat_1 = CategoricalAvelo(velo1)
	# cat_2 = CategoricalAvelo(velo2)
	# return str(cat_1) + "-" + str(cat_2)
	cat = CategoricalAvelo(velo1, velo2)
	return cat

def CategoricalAvelo(velo1, velo2):

	thres_dict = {0: [-10000, -7], 1: [-6, -6], 2: [-5, -5], 3: [-4, -4], 4: [-3, -3], 5: [-2, -2],
	              6: [-1, -1], 7: [0, 0], 8: [1, 1], 9: [2, 2], 10: [3, 3], 11: [4, 10000]}

	# velo = round(float(velo))
	# for key, value in thres_dict.items():
	# 	if velo >= value[0] and velo <= value[1]:
	# 		return key
	# 		break
	
	velo1 = float(velo1)
	velo2 = float(velo2)
	return round(velo1-velo2, 1)
	

if __name__ == '__main__':
	m1 = [ [ 2, 4 ], [ 2, 8 ], [ 6, 8 ], [ 6, 4 ] ]
	m2 = [ [ 3, 5 ], [ 3, 7 ], [ 5, 7 ], [ 5, 5 ] ]
	m1 = 2
	
	print(QASpARelation(m1, 4))

