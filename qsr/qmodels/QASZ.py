from __future__ import print_function, division
from shapely.geometry import Polygon


def QASZRelation(poly1, poly2):
	"""Calculate QASz relation of two polygons
	:param poly1: coordinate of polygon1 [[x1, y1], [x2, y2], ....[xn, yn]]
    :param poly2: coordinate of polygon2 [[x1, y1], [x2, y2], ....[xn, yn]]
	:return: QASz relation
	"""
	cat = CategoricalArea(Polygon(poly1).area, Polygon(poly2).area)
	# return str(cat)
	return cat


	
def CategoricalArea(area1, area2):
	# thres_list = [0, 51, 81, 111, 141, 146, 181, 211, 241, 271, 290, 301,
	#               331, 341, 349, 355, 375, 401, 431, 461, 491, 521, 551,
	#               581, 611, 641, 671, 701, 731, 751, 758, 785, 787, 791,
	#               806, 808, 817, 819, 821, 823, 851, 871, 901, 919, 938, 940,
	#               957, 973, 998, 1001, 1004, 1018, 1041, 1061, 1065, 1081, 1091,
	#               1095, 1098, 1102, 1111, 1141, 1144, 1150, 1160, 1191, 1200,
	#               1206, 1300, 1314, 10000]
	# thres_dict = {}
	# for i in range(len(thres_list)-1):
	# 	thres_dict[thres_list[i], thres_list[i+1]-1] = [i]
	# print(thres_dict)

	thres_dict = {0: [-9999, -501], 1: [-500, -451], 2: [-450, -401], 3: [-400, -251], 4: [-250, -216], 5: [-215, -211], 6: [-210, -161],
	              7: [-160, -141], 8: [-140, -121], 9: [-120, -101], 10: [-100, -72], 11: [-71, -70], 12: [-69, -39], 13: [-38, -37],
	              14: [-36, -34], 15: [-33, -31], 16: [-30, -1], 17: [0, 0], 18: [1, 30], 19: [31, 33], 20: [34, 36], 21: [37, 38],
	              22: [39, 69], 23: [70, 71], 24: [72, 100], 25: [101, 120], 26: [121, 140], 27: [141, 160], 28: [161, 210],
	              29: [211, 215], 30: [216, 250], 31: [251, 400], 32: [401, 450], 33: [451, 500], 34: [501, 9999]}

	diff_area = round(area1 - area2, 1)
	# for key, value in thres_dict.items():
	# 	if abs(diff_area)>=value[0] and abs(diff_area)<=value[1]:
	# 		if diff_area<0:
	# 			return key+ len(thres_dict)-1
	# 		else:
	# 			return key
	# 		break
	return diff_area

if __name__ == '__main__':
	m2 = [ [ 2, 4 ], [ 2, 8 ], [ 6, 8 ], [ 6, 4 ] ]
	m1 = [ [ 3, 5 ], [ 3, 7 ], [ 5, 7 ], [ 5, 5 ] ]
	
	print(QASZRelation(m1, m2))

