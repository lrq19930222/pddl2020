from __future__ import print_function, division
from shapely.geometry import Polygon
from math import sqrt
def SpeedRelation(data):
	"""
	Get qualitative speed difference between two objects
	input: [(x1, y1), (x2, y2)]
	output: speed difference index S
	"""
	thres_dict = {0: [-9999, -16], 1: [-15, -6], 2: [-5, -3], 3: [-2, -2], 4: [-1, -1], 5: [0, 0],
			   6: [1, 1], 7: [2, 2], 8: [3, 5], 9: [6, 15], 10: [16, 9999]}
	sp1 = sqrt(data[0][0]**2 + data[0][1]**2)
	sp2 = sqrt(data[1][0]**2 + data[1][1]**2)
	diff = abs(sp1-sp2)

# def QASPRelation(velo1, velo2):
# 	"""Calculate QASp relation of two polygons
# 	:param velo1: velocity of polygon1 [x,y]
#     :param velo2: velocity of polygon2 [x,y]
# 	:return: QASp relation
# 	"""
#
# 	cat = CategoricalVelo(velo1, velo2)
# 	# return str(cat)
# 	return cat
#
#
#
# def CategoricalVelo(velo1, velo2):
#
# 	# thresX_dict = {0: [-10000, -1], 1: [0, 0], 2: [1, 1], 3: [2, 2], 4: [3, 3], 5: [4, 4],
# 	#                6: [5, 5], 7: [6, 6], 8: [7, 7], 9: [8, 8], 10: [9, 9], 11: [10, 10], 12: [11, 10000]}
# 	# thresY_dict = {0: [-10000, -5], 1: [-4, -4], 2: [-3, -3], 3: [-2, -2], 4: [-1, -1], 5: [0, 0],
# 	#                6: [1, 1], 7: [2, 2], 8: [3, 3], 9: [4, 4], 10: [5, 5], 11: [6, 6], 12: [7, 10000]}
# 	thresX_dict = {0: [-9999, -17], 1: [-16, -7], 2: [-6, -3], 3: [-2, -2], 4: [-1, -1], 5: [0, 0],
# 	               6: [1, 1], 7: [2, 2], 8: [3, 6], 9: [7, 16], 10: [17, 9999]}
# 	thresY_dict = {0: [-9999, -16], 1: [-15, -6], 2: [-5, -3], 3: [-2, -2], 4: [-1, -1], 5: [0, 0],
# 	               6: [1, 1], 7: [2, 2], 8: [3, 5], 9: [6, 15], 10: [16, 9999]}
# 	cat_velo = ""
# 	# velo1 = [round(float(num)) for num in velo1]
# 	# velo2 = [round(float(num)) for num in velo2]
#
# 	# for key, value in thresX_dict.items():
# 	# 	if velo1[0]-velo2[0] >= value[0] and velo1[0]-velo2[0] <= value[1]:
# 	# 		cat_velo = str(key)
# 	# 		break
# 	#
# 	# for key, value in thresY_dict.items():
# 	# 	if velo1[1]-velo2[1] >= value[0] and velo1[1]-velo2[1] <= value[1]:
# 	# 		cat_velo = cat_velo + "-" + str(key)
# 	# 		break
# 	velo1 = [float(num) for num in velo1]
# 	velo2 = [float(num) for num in velo2]
# 	cat_velo = []
# 	cat_velo.append(round(velo1[0]-velo2[0], 1))
# 	cat_velo.append(round(velo1[1]-velo2[1], 1))
#
# 	return cat_velo

if __name__ == '__main__':
	m1 = [ [ 2, 4 ], [ 2, 8 ], [ 6, 8 ], [ 6, 4 ] ]
	m2 = [ [ 3, 5 ], [ 3, 7 ], [ 5, 7 ], [ 5, 5 ] ]
	m1 = [ 2, 4 ]
	m2 = [ 3, 4 ]
	print(QASPRelation(m1, m2))

