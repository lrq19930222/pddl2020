import os
import argparse
import glob
import pandas as pd


def merge_csv(data_path):
	"""
    merge the csv file for all the game levels
    :param data_path: the path store the individual csv files of all the game levels
    :return: merged data file
    """
	os.chdir(data_path)
	extension = 'csv'
	all_filenames = [ i for i in glob.glob('*.{}'.format(extension)) ]
	### only train on some levels
	combined_csv = pd.concat([pd.read_csv(f) for f in all_filenames])
	# export to csv
	# combined_csv.to_csv("combined.csv", index=False, encoding='utf-8')
	return combined_csv


def get_diff_rcc(df):
	"""
    get RCC difference between before and after state
    :param df: dataframe
    :return: categorical rcc difference value
    """
	# if df[ "rcc_start" ] == df[ "rcc_end" ]:
	# 	return 0
	# else:
	# 	return df[ "rcc_start" ] + "*" + df[ "rcc_end" ]
	return df[ "rcc_start" ] + "*" + df[ "rcc_end" ]


def get_diff_direct(df):
	"""
    get direct difference between before and after state
    :param df: dataframe
    :return: categorical direction difference value
    """

	return df["direct_start"] + "*" + df["direct_end"]
	
def get_diff_dist(df):
	"""
    get distance difference between before and after state
    :param df: dataframe
    :return: categorical distance difference value
    """

	return str(df["dist_start"]) + "*" + str(df["dist_end"])
	
	
def get_diff_cdr(df):
	"""
    get CDR difference between before and after state
    :param df: dataframe
    :return: categorical CDR difference value
    """
	if df[ "cdr_start" ] == df[ "cdr_end" ]:
		return 0
	else:
		b_s = df[ "cdr_start" ].split(":")
		a_s = df[ "cdr_end" ].split(":")
		s1 = list(set(b_s).difference(set(a_s)))
		s2 = list(set(a_s).difference(set(b_s)))
		return ':'.join(s1[ 0: ]) + "-" + ':'.join(s2[ 0: ])


def categorical_qaspa_diff(df, thres_qaspa_list):
	"""
    get QASpA difference between before and after state
    :param df: dataframe
    :return: categorical QASpA difference value
    """

	qaspa_diff_dict = {0: [-10000, thres_qaspa_list[0]], 1: [thres_qaspa_list[0]+1, 0], 2: [1, thres_qaspa_list[1]],
	                   3: [thres_qaspa_list[1]+1, 10000]}
	diff = round(float(df[ "qaspa_end" ]) - float(df[ "qaspa_start" ]))
	diff_start = round(float(df[ "qaspa_start" ]))
	diff_end = round(float(df[ "qaspa_end" ]))
	diff_a = ""
	for key, value in qaspa_diff_dict.items():
		if value[0] <= diff_start <= value[ 1 ]:
			diff_xa= str(key)
	for key, value in qaspa_diff_dict.items():
		if value[0] <= diff_end <= value[1]:
			diff_a = diff_a + "-" +str(key)
	return diff_a


def categorical_qaspx_diff(df, thres_qaspx_list):
	"""
    get QASpX difference between before and after state
    :param df: dataframe thres_qaspx:_list interval values
    :return: categorical QASpX difference value
    """

	qaspX_diff_dict = {0: [-10000, thres_qaspx_list[0]], 1: [thres_qaspx_list[0]+1, 0], 2: [1, thres_qaspx_list[1]],
	                   3: [thres_qaspx_list[1]+1, 10000]}
	diff_start = round(eval(df[ "qasp_start" ])[ 0 ])
	diff_end = round(eval(df[ "qasp_end" ])[ 0 ])
	diff_x = ""
	for key, value in qaspX_diff_dict.items():
		if value[ 0 ] <= diff_start <= value[ 1 ]:
			diff_x = str(key)
	for key, value in qaspX_diff_dict.items():
		if value[ 0 ] <= diff_end <= value[ 1 ]:
			diff_x = diff_x + "-" +str(key)
	return diff_x


def categorical_qaspy_diff(df, thres_qaspy_list):
	"""
	get QASpY difference between before and after state
    :param df: dataframe thres_qaspy_list interval values
    :return: categorical QASpY difference value
    """

	qaspY_diff_dict = {0: [-10000, thres_qaspy_list[0]], 1: [thres_qaspy_list[0]+1, 0], 2: [1, thres_qaspy_list[1]],
	                   3: [thres_qaspy_list[1]+1, 10000]}
	diff_start = round(eval(df[ "qasp_start" ])[ 1 ])
	diff_end = round(eval(df[ "qasp_end" ])[ 1 ])
	diff_y = ""
	for key, value in qaspY_diff_dict.items():
		if value[ 0 ] <= diff_start <= value[ 1 ]:
			diff_y = str(key)
	for key, value in qaspY_diff_dict.items():
		if value[ 0 ] <= diff_end <= value[ 1 ]:
			diff_y = diff_y + "-" +str(key)
	return diff_y


def one_hot_data(df, selected_feature=["rcc", "direct", "dist", "qtc", "exist"]):
	"""
	One-hot encoding of the selected features
	:param df: dataframe
	:param selected_feature: the categorical features that need to be one-hot encoded
	:return: 0/1 value
	"""
	for i in selected_feature:
		df = pd.concat([ df, pd.get_dummies(df[ "%s_diff" % i ], prefix="%s_diff" % i) ], axis=1)
		df = df.drop("%s_diff" % i, axis=1)
		#print(i, df.shape)
	return df
	
	
def get_diff_qtc(df):
	"""
    get QTC difference between before and after state
    :param df: dataframe
    :return: categorical distance difference value
    """
	qtc_start = df["qtc_start"]
	# qtc_start.append(df["qasp_start"])
	qtc_end = df["qtc_end"]
	# qtc_end.append(df["qasp_end"])
	# if str(qtc_start) == str(qtc_end):
	# 	return 0
	# else:
	# 	return str(qtc_start) + "*" + str(qtc_end)
	return str(qtc_start) + "*" + str(qtc_end)


def get_diff_exist(df):
	"""
    get existence difference between before and after state
    :param df: dataframe
    :return: categorical distance difference value
    """
	# if df["exist_start"] == df["exist_end"]:
	# 	return 0
	# else:
	# 	return df["exist_start"] + "*" + df["exist_end"]
	return df["exist_start"] + "*" + df["exist_end"]
		
	
def preprocess_data(data_path):
	"""
    merge all features into dataframe
    :param data_path: dataframe read path
    :return: df ready for clustering
    """
	df = merge_csv(data_path)
	df["rcc_diff"] = df.apply(lambda x: get_diff_rcc(x), axis=1)
	df[ "direct_diff" ] = df.apply(lambda x: get_diff_direct(x), axis=1)
	df["dist_diff"] = df.apply(lambda x: get_diff_dist(x), axis=1)
	# df[ "cdr_diff" ] = df.apply(lambda x: get_diff_cdr(x), axis=1)
	df["exist_diff"] = df.apply(lambda x: get_diff_exist(x), axis=1)
	# df[ "qaspa_diff" ] = df.apply(lambda x: categorical_qaspa_diff(x, thres_qaspa), axis=1)
	# df[ "qaspX_diff" ] = df.apply(lambda x: categorical_qaspx_diff(x, thres_qaspx), axis=1)
	# df[ "qaspY_diff" ] = df.apply(lambda x: categorical_qaspy_diff(x, thres_qaspy), axis=1)
	# df[ "qtc_start" ] = df["center_start"].apply(lambda x: get_qtc_feature(x))
	# df[ "qtc_end"] = df["center_end"].apply(lambda x: get_qtc_feature(x))
	df[ "qtc_diff"] = df.apply(lambda x: get_diff_qtc(x), axis=1)

	# remove_feature = ['rcc_start', 'rcc_end', 'dist_start', 'dist_end', 'direct_start', 'direct_end', 'center_start',
	#                   'center_end', 'cdr_start', 'cdr_end', 'qasz_start', 'qasz_end', 'qasp_start', 'qasp_end',
	#                   'qaspa_start', 'qaspa_end', 'exist_start', 'exist_end']
	remove_feature = ['rcc_start', 'rcc_end', 'dist_start', 'dist_end', 'direct_start', 'direct_end', 'qtc_start', 'qtc_end', 'exist_start', 'exist_end']
	df = df.drop(remove_feature, axis = 1)
	return df


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='STs generation')
	parser.add_argument('-r', '--readpath', help='Specify states path', required=True)
	parser.add_argument('-w', '--writepath', help='Specify STs write path', required=True)
	args = parser.parse_args()

	data_path = args.readpath
	write_path = args.writepath

	df = preprocess_data(data_path=data_path)
	df.to_csv(write_path, index=False)
