from collections import Counter
import datetime
from qsr.Distance_neighborhood import calculate_neighborhood_distance


def graph_clustering(data, graph, num, constraint):
	"""
	check if a st can be merged to any graph
	:param data: st
	:param graph: existing graphs
	:param num: num = the number of features used for calculating distance + 1(type pair)
	:param constraint: only when distance (st_1, st_2) >= constraint, they are neighbors
	:return: true/false: one st can be merged to one of the graphs
	"""
	flag_match = False
	for i in graph:
		if data[0][-1] == i[-1]:
			count = 0
			for j in range(len(data[0])):
				if data[0][j] == i[j]:
					count += 1
			if num == 5 and count >= num:
				if calculate_neighborhood_distance(data[0], i) <= constraint:
					flag_match = True
					break
			if num == 6 and count >= num:
				flag_match = True
				break


	return flag_match
	

def get_cluster(data, num, constraint):
	"""
	get initial clusters from state transitions
	:param data: list of state transitions
	:param num: num = the number of features used for calculating distance + 1(type pair)
	:param constraint: only when distance (st_1, st_2) >= constraint, they are neighbors
	:return: a list of initial clusters
	"""
	graph = {}
	for i in range(len(data)):
		if i == 0:
			graph[0] = data[i]
		else:
			flag = True
			for j in graph.keys():
				if graph_clustering(data[i], graph[j], num, constraint):
					graph[j].append(data[i][0])
					flag = False
					break
			if flag:
				graph[len(graph)] = data[i]
		# if i%100000 == 0:
		# 	print("Finished {}th data, time is {}".format(i, datetime.datetime.now()))
	print("After processing {} data, the number of clusters is {}".format(len(data), len(graph)))

	return list(graph.values())
		

		
